import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import { User, UsersHttpModule } from "@app/users-http";
import { TypeOrmCoreModule } from "@nestjs/typeorm/dist/typeorm-core.module";
import { SignupHttpModule } from "@app/signup-http";
import { EventEmitterModule } from "@nestjs/event-emitter";
import { Password } from "@app/passwords";
import { ConfigModule, DatabaseConfiguration } from "@app/config";
import { ConfigService } from "@nestjs/config";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";
import { WalletsHttpModule } from "@app/wallets-http";
import { Wallet } from "@app/wallets/entity/wallet";
import { IndexController } from './controller/index/index.controller';
import { Developer } from "@app/developers/entity/developer";
import { DevelopersHttpModule } from "@app/developers-http";
import { DevelopersProductHttpModule } from "@app/developers-product-http";
import { AuthenticationHttpModule } from "@app/authentication-http";
import { APP_INTERCEPTOR } from "@nestjs/core";

@Module({
    imports: [
        TypeOrmCoreModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: (config: ConfigService) => {
                const db = config.get<DatabaseConfiguration>('database');
                return {
                    type: "postgres",
                    username: db.postgres.user,
                    password: db.postgres.password,
                    database: db.postgres.database,
                    host: db.postgres.host,
                    port: db.postgres.port,
                    entities: [
                        User,
                        Password,
                        Wallet,
                        Developer
                    ]
                } as PostgresConnectionOptions
            },
            inject: [ConfigService],
        }),
        AuthenticationHttpModule,
        EventEmitterModule.forRoot(),
        UsersHttpModule,
        SignupHttpModule,
        ConfigModule,
        WalletsHttpModule,
        DevelopersHttpModule,
        DevelopersProductHttpModule,
    ],
    controllers: [IndexController],
    providers: [
        {
            provide: APP_INTERCEPTOR,
            useClass: ClassSerializerInterceptor,
        }
    ]
})
export class AppModule {}
