import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { UsersHttpSwagger } from "@app/users-http/users-http-swagger";
import { SignupHttpSwagger } from "@app/signup-http/signup-http-swagger";
import { ConfigService } from "@nestjs/config";
import { HttpConfiguration, StartupConfiguration } from "@app/config";
import { WalletHttpSwagger } from "@app/wallets-http";
import { NestExpressApplication } from "@nestjs/platform-express";
import { join } from "path";
import { DevelopersHttpSwagger } from "@app/developers-http";
import { DevelopersProductHttpSwagger } from "@app/developers-product-http/developers-product-http-swagger";
import { AuthenticationHttpSwagger } from "@app/authentication-http/authentication-http-swagger";

(async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.setBaseViewsDir(join(__dirname, "..", "..", 'views'));
  app.setViewEngine('hbs');
  const swaggers = [
    UsersHttpSwagger,
    SignupHttpSwagger,
    WalletHttpSwagger,
    DevelopersHttpSwagger,
    DevelopersProductHttpSwagger,
    AuthenticationHttpSwagger
  ];
  swaggers.forEach(fn => fn(app));
  const config = app.get(ConfigService);
  const configuration: StartupConfiguration&HttpConfiguration = {
    ...config.get<StartupConfiguration>("startup"),
    ...config.get<HttpConfiguration>('http'),
  };
  setTimeout(async () => {
    await app.listen(configuration.port);
    console.log(`Application is running on port ${configuration.host}:${configuration.port}`);
  }, configuration.timeout);
})()
