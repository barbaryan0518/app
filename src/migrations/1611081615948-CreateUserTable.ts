import { MigrationInterface, QueryRunner, Table } from "typeorm";

/**
 * @migration
 * @class
 */
export class CreateUserTable1611081615948 implements MigrationInterface {

    /**
     *
     * @param queryRunner
     */
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"')
        await queryRunner.createTable(new Table({
            name: "users",
            columns: [
                {
                    name: 'id',
                    type: 'uuid',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: 'login',
                    type: 'character varying',
                    length: '255',
                    isUnique: true,
                }
            ]
        }));
    }

    /**
     *
     * @param queryRunner
     */
    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("users", true);
    }

}
