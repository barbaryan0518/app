import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

/**
 * @migration
 * @class
 */
export class CreateDevelopersTable1611428249904 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'developers',
            columns: [
                {
                    name: 'id',
                    type: 'uuid',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid'
                },
                {
                    name: 'avatar',
                    type: 'character',
                    length: '255',
                },
                {
                    name: 'name',
                    type: 'character',
                    length: '255',
                },
                {
                    name: 'price',
                    type: 'numeric',
                    precision: 10,
                    scale: 2,
                },
                {
                    name: 'performance',
                    type: 'int',
                },
                {
                    name: 'users_id',
                    type: 'uuid'
                }
            ]
        });
        await queryRunner.createTable(table);
        await queryRunner.createForeignKey(table, new TableForeignKey({
            columnNames: ["users_id"],
            onUpdate: "CASCADE",
            onDelete: "CASCADE",
            referencedTableName: "users",
            referencedColumnNames: ["id"]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const table = await queryRunner.getTable("developers");
        const fk = table.foreignKeys.find(fk => fk.columnNames.indexOf("users_id") !== -1);
        await queryRunner.dropForeignKey(table, fk);
        await queryRunner.dropTable(table);
    }

}
