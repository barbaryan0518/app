import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

/**
 * @class
 */
export class CreatePasswordTable1611252201755 implements MigrationInterface {

    /**
     *
     * @param queryRunner
     */
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'users_passwords',
            columns: [
                {
                    name: 'id',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                    isGenerated: true,
                    type: 'uuid',
                },
                {
                    name: 'password',
                    type: 'varchar',
                    length: '255',
                },
                {
                    name: 'users_id',
                    type: 'uuid'
                }
            ]
        }));
        await queryRunner.createForeignKey('users_passwords', new TableForeignKey({
            columnNames: ['users_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: "CASCADE",
            onUpdate: "CASCADE"
        }))
    }

    /**
     *
     * @param queryRunner
     */
    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("users_passwords", true);
    }

}
