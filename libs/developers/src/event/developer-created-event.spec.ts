import { DeveloperCreatedEvent } from './developer-created-event';
import { Developer } from "@app/developers/entity/developer";

describe('DeveloperCreatedEvent', () => {
  it('should be defined', () => {
    expect(new DeveloperCreatedEvent({} as Developer)).toBeDefined();
  });
});
