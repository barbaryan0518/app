import { Developer } from "@app/developers/entity/developer";

export class DeveloperCreatedEvent {
    public static readonly event: string = 'developer.created';

    constructor(
        private readonly developer: Developer
    ) {}

    public getDeveloper(): Developer {
        return this.developer;
    }
}
