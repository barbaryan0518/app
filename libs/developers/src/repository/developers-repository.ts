import { Connection, Repository } from "typeorm";
import { Injectable } from "@nestjs/common";
import { Developer } from "@app/developers/entity/developer";
import { DevelopersRepositoryInterface } from "@app/developers/repository/developers-repository.interface";
import { DeveloperByIdNotFoundException } from "@app/developers/exception/developer-by-id-not-found-exception";

@Injectable()
export class DevelopersRepository implements DevelopersRepositoryInterface {

    private readonly repository: Repository<Developer> = this.connection.getRepository(Developer);

    constructor(
        private readonly connection: Connection
    ) {}

    public async find(options?: undefined): Promise<Developer[]> {
        return await this.repository.find();
    }

    public async findByOwnerId(ownerId: string): Promise<Developer[]> {
        return await this.repository.find({
            where: {
                owner: {
                    id: ownerId
                }
            }
        })
    }

    public async getById(id: string): Promise<Developer> {
        const found = await this.repository.findOne(id);
        if(!found) {
            throw new DeveloperByIdNotFoundException(id);
        }
        return found;
    }

    public async remove(developer: Developer): Promise<void> {
        await this.repository.remove(developer);
    }

    public async save(developer: Developer): Promise<void> {
        await this.repository.save(developer);
    }

    public async update(developer: Developer): Promise<void> {
        await this.repository.save(developer);
    }
}
