import { Inject } from "@nestjs/common";
import { Developer } from "@app/developers/entity/developer";

export interface DevelopersRepositoryInterface {
    getById(id: string): Promise<Developer>;
    findByOwnerId(ownerId: string): Promise<Developer[]>;
    find(options?: undefined): Promise<Developer[]>;
    save(developer: Developer): Promise<void>;
    update(developer: Developer): Promise<void>;
    remove(developer: Developer): Promise<void>;
}

export const DEVELOPERS_REPOSITORY_INTERFACE = "DevelopersRepositoryInterface";

export const InjectDevelopersRepository = Inject.bind(null, DEVELOPERS_REPOSITORY_INTERFACE);
