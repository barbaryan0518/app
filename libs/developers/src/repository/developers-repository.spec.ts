import { DevelopersRepository } from './developers-repository';
import { Connection } from "typeorm";

describe('DevelopersRepository', () => {
  it('should be defined', () => {
    const connection = {
      getRepository: () => ({})
    } as unknown as Connection;
    expect(new DevelopersRepository(connection)).toBeDefined();
  });
});
