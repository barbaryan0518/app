import { Injectable, NotImplementedException } from '@nestjs/common';
import {
    DevelopersRepositoryInterface,
    InjectDevelopersRepository
} from "@app/developers/repository/developers-repository.interface";
import { Developer } from "@app/developers/entity/developer";
import { CreateDeveloperDto } from "@app/developers/dto/create-developer-dto";
import { UsersService } from "@app/users";
import { v4 } from "uuid";
import { EventEmitter2 } from "@nestjs/event-emitter";
import { DeveloperCreatedEvent } from "@app/developers/event/developer-created-event";

@Injectable()
export class DevelopersService {
    constructor(
        @InjectDevelopersRepository() private readonly developers: DevelopersRepositoryInterface,
        private readonly users: UsersService,
        private readonly emitter: EventEmitter2
    ) {}

    public async create(dto: CreateDeveloperDto): Promise<Developer> {
        const owner = await this.users.getById(dto.owner);
        const id = dto.id || v4();
        const developer = new Developer(id, dto.name, dto.avatar, dto.performance, dto.price, owner);
        await this.developers.save(developer);
        await this.emitter.emitAsync(DeveloperCreatedEvent.event, new DeveloperCreatedEvent(developer));
        return developer;
    }

    public async getByOwnerId(ownerId: string): Promise<Developer[]> {
        return await this.developers.findByOwnerId(ownerId);
    }

    public async find(options?: undefined): Promise<Developer[]> {
        return await this.developers.find(options);
    }

    public async remove(id: string): Promise<void> {
        const developer = await this.developers.getById(id);
        await this.developers.remove(developer);
    }
}
