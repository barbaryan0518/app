import { CreateDeveloperDto } from './create-developer-dto';

describe('CreateDeveloperDto', () => {
  it('should be defined', () => {
    expect(new CreateDeveloperDto()).toBeDefined();
  });
});
