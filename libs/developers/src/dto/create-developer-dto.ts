import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsNumber, IsOptional, IsString, IsUUID } from "class-validator";

export class CreateDeveloperDto {
    @ApiProperty({
        required: false,
        type: 'string',
        format: 'uuid'
    })
    @IsUUID('4')
    @IsOptional()
    id?: string;

    @ApiProperty({
        required: true,
        type: 'string',
    })
    @IsString()
    name: string;

    @ApiProperty({
        type: 'string'
    })
    @IsString()
    avatar: string;

    @ApiProperty()
    @IsInt()
    performance: number;

    @ApiProperty()
    @IsNumber()
    price: number;

    @ApiProperty({
        type: 'string',
        format: 'uuid'
    })
    @IsUUID('4')
    owner: string;
}
