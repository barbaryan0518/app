import { Module } from '@nestjs/common';
import { WalletsController } from './controller/wallets/wallets.controller';
import { WalletsModule } from "@app/wallets";
import { AuthenticationModule } from "@app/authentication";

@Module({
  imports: [
      WalletsModule,
      AuthenticationModule,
  ],
  controllers: [WalletsController],
})
export class WalletsHttpModule {}
