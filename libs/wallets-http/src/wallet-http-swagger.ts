import { INestApplication } from "@nestjs/common";
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from "@nestjs/swagger";
import { WalletsHttpModule } from "@app/wallets-http/wallets-http.module";

export function WalletHttpSwagger(app: INestApplication): OpenAPIObject {
    const options = new DocumentBuilder()
        .setTitle("Wallets")
        .addBearerAuth()
        .setVersion("1.0")
        .build();
    const document = SwaggerModule.createDocument(app, options, {
        include: [
            WalletsHttpModule,
        ]
    });
    SwaggerModule.setup("api/wallets/docs", app, document);
    return document;
}
