import { LoginResult } from './login-result';

describe('LoginResult', () => {
  it('should be defined', () => {
    expect(new LoginResult("str")).toBeDefined();
  });
});
