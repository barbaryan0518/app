import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";

export class LoginResult {

    @ApiProperty()
    @Expose()
    public readonly accessToken: string;

    constructor(token: string) {
        this.accessToken = token;
    }
}
