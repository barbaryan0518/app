import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationService } from './authentication.service';
import { UsersService } from "@app/users";
import { PasswordsService } from "@app/passwords";
import { JwtService } from "@nestjs/jwt";

describe('AuthenticationService', () => {
  let service: AuthenticationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
          AuthenticationService,
          {
              provide: UsersService,
              useValue: {},
          },
          {
            provide: PasswordsService,
            useValue: {},
          },
          {
            provide: JwtService,
            useValue: {},
          },
      ],
    }).compile();

    service = module.get<AuthenticationService>(AuthenticationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
