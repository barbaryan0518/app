import { Injectable } from '@nestjs/common';
import { User, UsersService } from "@app/users";
import { JwtService } from "@nestjs/jwt";
import { PasswordsService } from "@app/passwords";
import { LoginResult } from "@app/authentication/model/login-result";
import { classToPlain } from "class-transformer";

@Injectable()
export class AuthenticationService {
    constructor(
        private readonly users: UsersService,
        private readonly passwords:  PasswordsService,
        private readonly jwt: JwtService
    ) {}

    public async validateUser(login: string, password: string): Promise<User|undefined> {
        const user = await this.users.findByLogin(login);
        if(!user) {
            return undefined;
        }
        const passwords = await this.passwords.findByUserId(user.getId());
        if(passwords.length && passwords.some(item => item.compare(password))) {
            return user;
        }
        return undefined;
    }

    public async login(user: User): Promise<LoginResult> {
        return new LoginResult(
            await this.jwt.sign(classToPlain(user)),
        );
    }
}
