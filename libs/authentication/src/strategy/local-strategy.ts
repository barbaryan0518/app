import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { AuthenticationService } from "@app/authentication/service/authentication.service";
import { User } from "@app/users";
import { Injectable, UnauthorizedException } from "@nestjs/common";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly authentication: AuthenticationService
    ) {
        super();
    }

    public async validate(username: string, password: string): Promise<User> {
        const user = await this.authentication.validateUser(username, password);
        if(!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}
