import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { Injectable } from "@nestjs/common";
import { User } from "@app/users";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        config: ConfigService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: config.get<string>('auth.secret')
        });
    }

    public async validate(payload: User): Promise<User> {
        delete payload["iat"];
        Reflect.setPrototypeOf(payload, User.prototype);
        return payload;
    }
}
