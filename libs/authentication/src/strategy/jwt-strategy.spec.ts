import { JwtStrategy } from './jwt-strategy';

describe('JwtStrategy', () => {
  it('should be defined', () => {
    expect(new JwtStrategy({ get: () => 'str' } as never)).toBeDefined();
  });
});
