import { BasicStrategy } from './basic-strategy';

describe('BasicStrategy', () => {
  it('should be defined', () => {
    expect(new BasicStrategy({} as never)).toBeDefined();
  });
});
