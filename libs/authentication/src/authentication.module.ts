import { Module } from '@nestjs/common';
import { AuthenticationService } from './service/authentication.service';
import { UsersModule } from "@app/users";
import { PasswordsModule } from "@app/passwords";
import { JwtModule } from "@nestjs/jwt";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { LocalStrategy } from "@app/authentication/strategy/local-strategy";
import { JwtStrategy } from "@app/authentication/strategy/jwt-strategy";
import { PassportModule } from "@nestjs/passport";
import { BasicStrategy } from "@app/authentication/strategy/basic-strategy";

@Module({
  imports: [
      ConfigModule,
      UsersModule,
      PasswordsModule,
      PassportModule,
      JwtModule.registerAsync({
          inject: [ConfigService],
          useFactory: (config: ConfigService) => {
              return {
                  secret: config.get('auth.secret'),
              }
          }
      })
  ],
  providers: [
      AuthenticationService,
      LocalStrategy,
      JwtStrategy,
      BasicStrategy,
  ],
  exports: [
      AuthenticationService,
      LocalStrategy,
      JwtModule,
      BasicStrategy,
  ],
})
export class AuthenticationModule {}
