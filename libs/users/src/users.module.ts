import { Module } from '@nestjs/common';
import { UsersService } from './service/users.service';
import { UsersRepository } from "@app/users/repository/users-repository";
import { USERS_REPOSITORY_INTERFACE } from "@app/users/repository/users-repository.interface";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "@app/users/entity/user";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            User
        ])
    ],
    providers: [
        UsersService,
        UsersRepository,
        {
            provide: USERS_REPOSITORY_INTERFACE,
            useExisting: UsersRepository,
        }
    ],
    exports: [
        UsersService,
        UsersRepository,
        {
            provide: USERS_REPOSITORY_INTERFACE,
            useExisting: UsersRepository,
        }
    ],
})
export class UsersModule {}
