import { User } from "@app/users/entity/user";
import { Inject } from "@nestjs/common";

export interface UsersRepositoryInterface {
    save(user: User): Promise<void>;
    findByLogin(login: string): Promise<User|undefined>;
    getById(id: string): Promise<User>;
    remove(user: User): Promise<void>;
}

export const USERS_REPOSITORY_INTERFACE = "UsersRepositoryInterface";

export function InjectUsersRepository(): any {
    return Inject(USERS_REPOSITORY_INTERFACE);
}
