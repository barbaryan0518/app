import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";

export class CreateUserDto {
    @ApiProperty({
        name: 'id',
        required: false,
    })
    @IsString()
    @IsOptional()
    id?: string;

    @ApiProperty()
    @IsString()
    login: string;
}
