import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { CreateUserDto } from "@app/users/dto/create-user-dto";
import { User } from "@app/users/entity/user";
import { USERS_REPOSITORY_INTERFACE, UsersRepositoryInterface } from "@app/users/repository/users-repository.interface";
import { isUUID } from "@nestjs/common/utils/is-uuid";
import { UserLoginAlreadyExistsException } from "@app/users/exception/user-login-already-exists-exception";

describe('UsersService', () => {
  let service: UsersService;
  let usersRepository: UsersRepositoryInterface;

  beforeEach(async () => {
    usersRepository = {
      save: jest.fn().mockImplementation(),
      getById: jest.fn().mockImplementation(),
      remove: jest.fn(),
      findByLogin: jest.fn().mockImplementation(async () => undefined)
    }
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: USERS_REPOSITORY_INTERFACE,
          useValue: usersRepository
        }
      ],
    })
        .compile();

    service = module.get(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe("create", () => {
    it("should be create user", async () => {
      const dto: CreateUserDto = {
        login: 'test',
        id: 'same'
      };
      const user = await service.create(dto);
      expect(user).toBeDefined();
      expect(user).toBeInstanceOf(User);
      expect(user.getId()).toBe(dto.id);
      expect(user.getLogin()).toBe(dto.login);
    });
    it("should be generate user id as uuid.v4", async () => {
      const dto: CreateUserDto = {
        login: "test",
      };
      const user = await service.create(dto);
      expect(isUUID(user.getId())).toBeTruthy();
    });
    it("should be throw user already exists exception", async () => {
      usersRepository.findByLogin = jest.fn().mockImplementation(() => ({}));
      const dto: CreateUserDto = { login: "same" };
      try {
        await service.create(dto);
      } catch (e) {
        expect(e).toBeInstanceOf(UserLoginAlreadyExistsException);
        expect(e.login).toBe(dto.login);
        expect(e.message).toBe(`Login ${dto.login} already exists`);
      }
    })
  });
});
