import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "@app/users";
import { Exclude } from "class-transformer";
import * as bcrypt from "bcrypt";

@Entity("users_passwords")
export class Password {


    @PrimaryGeneratedColumn('uuid')
    @Exclude()
    private readonly id: string;

    @Column()
    @Exclude()
    private password: string;

    @ManyToOne(() => User, {
        eager: true,
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
    })
    @JoinColumn({
        referencedColumnName: 'id',
        name: 'users_id'
    })
    @Exclude()
    private readonly user: User;

    private static readonly rounds: number = 10;

    constructor(id: string, password: string, user: User) {
        this.id = id;
        if(password) {
            this.password = bcrypt.hashSync(password, Password.rounds);
        }
        this.user = user;
    }

    public getId(): string {
        return this.id;
    }

    public getValue(): string {
        return this.password;
    }

    public compare(password: string): boolean {
        return bcrypt.compareSync(password, this.password);
    }

    public change(password: string): void {
        this.password = bcrypt.hashSync(password, Password.rounds);
    }

    public getUser(): User {
        return this.user;
    }
}
