import { Password } from './password';
import { User } from "@app/users";

describe('Password', () => {
  it('should be defined', () => {
    expect(new Password("id", "value", {} as User)).toBeDefined();
  });

  describe("create", () => {
    it("should be create hashed password", async () => {
      const pass = "test";
      const user = {} as User;
      const password = new Password("id", pass, user);

      expect(password.getId()).toBe("id");
      expect(password.getUser()).toEqual(user);
      expect(password.getValue()).not.toBe(pass);
    });

    it("should be compare password", async () => {
      const password = new Password("id", "test", {} as User);

      expect(password.compare("test")).toBeTruthy();
      expect(password.compare("same")).toBeFalsy();
    });

    it("should be create with undefined without errors", () => {
      const password = new Password(undefined, undefined, undefined);

      expect(password).toBeDefined();
    });
  });
});
