import { CreatePasswordDto } from './create-password-dto';

describe('CreatePasswordDto', () => {
  it('should be defined', () => {
    expect(new CreatePasswordDto()).toBeDefined();
  });
});
