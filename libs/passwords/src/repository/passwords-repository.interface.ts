import { Inject } from "@nestjs/common";
import { Password } from "@app/passwords/entity/password";

export interface PasswordsRepositoryInterface {
    save(password: Password): Promise<void>;
    findByUserId(userId: string): Promise<Password[]>;
    update(password: Password): Promise<void>;
}

export const PASSWORDS_REPOSITORY = "PasswordsRepositoryInterface";
export function InjectPasswordsRepository(): any {
    return Inject(PASSWORDS_REPOSITORY);
}
