import { Module } from '@nestjs/common';
import { PasswordsService } from './service/passwords.service';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Password } from "@app/passwords/entity/password";
import { UsersModule } from "@app/users";
import { PasswordsRepository } from "@app/passwords/repository/passwords-repository";
import { PASSWORDS_REPOSITORY } from "@app/passwords/repository/passwords-repository.interface";

@Module({
  imports: [
      TypeOrmModule.forFeature([
          Password
      ]),
      UsersModule,
  ],
  providers: [
      PasswordsService,
      PasswordsRepository,
      {
          provide: PASSWORDS_REPOSITORY,
          useExisting: PasswordsRepository,
      }
  ],
  exports: [
      PasswordsService,
      PasswordsRepository,
      {
          provide: PASSWORDS_REPOSITORY,
          useExisting: PasswordsRepository,
      }
  ],
})
export class PasswordsModule {}
