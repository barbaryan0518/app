import { Test, TestingModule } from '@nestjs/testing';
import { PasswordsService } from './passwords.service';
import { CreatePasswordDto } from "@app/passwords/dto/create-password-dto";
import { User, UsersService } from "@app/users";
import {
  InjectPasswordsRepository, PASSWORDS_REPOSITORY,
  PasswordsRepositoryInterface
} from "@app/passwords/repository/passwords-repository.interface";
import { Password } from "@app/passwords/entity/password";

describe('PasswordsService', () => {
  let service: PasswordsService;
  let usersService: UsersService;
  let passwordsRepository: PasswordsRepositoryInterface;

  beforeEach(async () => {
    usersService = {} as UsersService;
    passwordsRepository = {
      save: jest.fn().mockImplementation() as unknown,
    } as PasswordsRepositoryInterface;
    const module: TestingModule = await Test.createTestingModule({
      providers: [
          PasswordsService,
          {
            provide: PASSWORDS_REPOSITORY,
            useValue: passwordsRepository,
          },
          {
            provide: UsersService,
            useValue: usersService
          }
      ],
    }).compile();

    service = module.get<PasswordsService>(PasswordsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe("create", () => {
    it("should be create password", async () => {
      const dto = {
        user: "guid",
        value: "same",
        id: "1234"
      } as CreatePasswordDto;
      const user = {
        id: "same"
      } as unknown as User;
      usersService.getById = jest.fn().mockImplementation(() => user);
      passwordsRepository.save = jest.fn().mockImplementation();
      const password = await service.create(dto);
      expect(password.getId()).toBe(dto.id);
      expect(password.getValue()).not.toBe(dto.value);
      expect(password.getUser()).toEqual(user);
      expect(passwordsRepository.save).toHaveBeenCalled();
    });
  });

  describe("findByUserId", () => {
    it("should be return array of passwords by user id", async () => {
      const data = [
          new Password("1234", "1234", {} as User),
          new Password("222", "321", {} as User),
      ];
      passwordsRepository.findByUserId = jest.fn().mockImplementation(() => data);
      const passwords = await service.findByUserId("123");
      expect(passwords).toEqual(data);
    });
  })
});
