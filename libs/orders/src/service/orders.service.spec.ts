import { Test, TestingModule } from '@nestjs/testing';
import { OrdersService } from './orders.service';
import { WalletsService } from "@app/wallets/service/wallets.service";
import { DeveloperProduct } from "./../../../developers-product/src/entity/developer-product";
import { DevelopersService } from "@app/developers/service/developers.service";
import { BuyDeveloperDto } from "@app/orders/dto/buy-developer-dto";
import { Wallet } from "@app/wallets/entity/wallet";
import { User } from "@app/users/entity/user";
import { CreateDeveloperDto } from "@app/developers/dto/create-developer-dto";
import { Developer } from "@app/developers/entity/developer";
import { v4 } from "uuid";
import { WithdrawWalletDto } from "@app/wallets/dto/withdraw-wallet-dto";
import { DevelopersProductService } from "./../../../developers-product/src/service/developers-product.service";


describe('OrdersService', () => {
  let orders: OrdersService;
  let wallets: WalletsService;
  let products: DevelopersProductService;
  let developers: DevelopersService;

  beforeAll(async () => {
    wallets = {} as unknown as WalletsService;
    products = {} as unknown as DevelopersProductService;
    developers = {} as unknown as DevelopersService;

    const module: TestingModule = await Test.createTestingModule({
      providers: [
          {
            provide: OrdersService,
            useFactory: (w, dp, d) => new OrdersService(w, dp, d),
            inject: [WalletsService, DevelopersProductService, DevelopersService]
          },
          { provide: WalletsService, useValue: wallets },
          { provide: DevelopersProductService, useValue: products },
          { provide: DevelopersService, useValue: developers },
      ],
    })
        .compile();

    orders = module.get<OrdersService>(OrdersService);
  });

  it('should be defined', () => {
    expect(orders).toBeDefined();
  });

  it('should be buy wallets', async () => {
    const dto = {
      user: 'ususs--sss',
      products: ['123', '322']
    } as BuyDeveloperDto;
    const wallet = Wallet.create("ddd", {} as User, 100)
    wallets.getByUserId = jest.fn().mockImplementation(async uid => {
      expect(uid).toBe(dto.user);
      return [wallet];
    });
    products.findByIds = jest.fn().mockImplementation(async ids => {
      expect(ids).toEqual(dto.products);
      return [
          new DeveloperProduct("123", "test", "ava", 300, 100),
          new DeveloperProduct("322", "test2", "ava2", 150, 50),
      ];
    });
    developers.create = jest.fn().mockImplementation(async (args: CreateDeveloperDto) => {
      expect(["test", "test2"]).toContain(args.name);
      expect(args.owner).toBe(dto.user);
      if(args.name === "test") {
        expect(args.performance).toBe(300);
        expect(args.price).toBe(100);
        expect(args.avatar).toBe("ava");
      }
      if(args.name === "test2") {
        expect(args.performance).toBe(150);
        expect(args.price).toBe(50);
        expect(args.avatar).toBe("ava2");
      }
      return new Developer(v4(), args.name, args.avatar, args.performance, args.price, {} as User);
    });
    wallets.withdraw = jest.fn().mockImplementation(async (id: string, dto: WithdrawWalletDto) => {
      expect(id).toBe(wallet.getId());
      expect([50, 100]).toContainEqual(dto.amount);
      return wallet;
    });
    const result = await orders.buy(dto);
    expect(result).toHaveLength(2);
  });
});
