import { Module } from '@nestjs/common';
import { SignupController } from './controller/signup/signup.controller';
import { SignupModule } from "@app/signup";

@Module({
  imports: [
      SignupModule,
  ],
  exports: [
      SignupModule
  ],
  controllers: [SignupController],
})
export class SignupHttpModule {}
