import { INestApplication } from "@nestjs/common";
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from "@nestjs/swagger";
import { SignupHttpModule } from "@app/signup-http/signup-http.module";

export function SignupHttpSwagger(app: INestApplication): OpenAPIObject {
    const options = new DocumentBuilder()
        .setTitle("Sign up")
        .setVersion("1.0")
        .build();
    const document = SwaggerModule.createDocument(app, options, {
        include: [
            SignupHttpModule,
        ]
    });
    SwaggerModule.setup("api/signup/docs", app, document);
    return document;
}
