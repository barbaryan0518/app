import { Global, Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from "@nestjs/config";
import { ValidationSchema } from "@app/config/validation/validation-schema";
import * as yaml from "js-yaml";
import * as fs from "fs";
import * as path from "path";

@Global()
@Module({
    imports: [
        NestConfigModule.forRoot({
            load: [
                (): any => {
                    const string = fs.readFileSync(path.join(__dirname, "../../../..", "config.yaml"), 'utf8');
                    const data = yaml.load(string);
                    if(typeof data !== 'object') {
                        throw new Error("Cannot load config from config.yaml");
                    }
                    // console.log(data, string);
                    new ValidationSchema().validate(data as never);
                    return data;
                },
            ]
        }),
    ],
    exports: [
        NestConfigModule,
    ]
})
export class ConfigModule {}
