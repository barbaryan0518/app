export interface PostgresConfiguration {
    user: string;
    password: string;
    database: string;
    host: string;
    port: number;
}
