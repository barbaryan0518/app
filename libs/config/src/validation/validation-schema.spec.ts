import { ValidationSchema } from './validation-schema';

describe('ValidationSchema', () => {
  it('should be defined', () => {
    expect(new ValidationSchema()).toBeDefined();
  });
});
