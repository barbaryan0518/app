import { INestApplication } from "@nestjs/common";
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from "@nestjs/swagger";
import { DevelopersHttpModule } from "@app/developers-http/developers-http.module";

export function DevelopersHttpSwagger(app: INestApplication): OpenAPIObject {
    const options = new DocumentBuilder()
        .setTitle("Developers")
        .addBearerAuth()
        .setVersion("1.0")
        .build();
    const document = SwaggerModule.createDocument(app, options, {
        include: [
            DevelopersHttpModule,
        ]
    });
    SwaggerModule.setup("api/developers/docs", app, document);
    return document;
}
