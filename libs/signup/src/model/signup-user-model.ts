import { User } from "@app/users";
import { Password } from "@app/passwords";
import { Exclude, Expose } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

export class SignupUserModel {
    @Exclude()
    private readonly user: User;
    @Exclude()
    private readonly password: Password;

    constructor(user: User, password: Password) {
        this.user = user;
        this.password = password;
    }

    @ApiProperty({
        type: User,
        name: 'user'
    })
    @Expose({
        name: 'user'
    })
    public getUser(): User {
        return this.user;
    }

    public getPassword(): Password {
        return this.password;
    }
}
