import { Injectable } from '@nestjs/common';
import { SignupUserDto } from "@app/signup/dto/signup-user-dto";
import { UsersService } from "@app/users";
import { PasswordsService } from "@app/passwords";
import { SignupUserModel } from "@app/signup/model/signup-user-model";
import { EventEmitter2 } from "@nestjs/event-emitter";
import { SignupUserEvent } from "@app/signup/event/signup-user-event";

@Injectable()
export class SignupService {

    constructor(
        private readonly users: UsersService,
        private readonly passwords: PasswordsService,
        private readonly emitter: EventEmitter2
    ) {}

    public async signup(dto: SignupUserDto): Promise<SignupUserModel> {
        const user = await this.users.create({
            login: dto.login
        });
        try {
            const password = await this.passwords.create({
                value: dto.password,
                user: user.getId()
            });
            await this.emitter.emitAsync(SignupUserEvent.event, new SignupUserEvent(user, password));
            return new SignupUserModel(user, password);
        } catch (e) {
            await this.users.remove(user);
            throw e;
        }
    }

}
