import { User } from "@app/users";
import { Password } from "@app/passwords";

export class SignupUserEvent {

    public static readonly event: string = 'signup.user';

    constructor(
        public readonly user: User,
        public readonly password: Password,
    ) {}
}
