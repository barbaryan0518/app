import { SignupUserEvent } from './signup-user-event';
import { Password } from "@app/passwords";
import { User } from "@app/users";

describe('SignupUserEvent', () => {
  it('should be defined', () => {
    expect(new SignupUserEvent({} as User, {} as Password)).toBeDefined();
  });
});
