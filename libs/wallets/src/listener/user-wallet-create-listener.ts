import { OnEvent } from "@nestjs/event-emitter";
import { SignupUserEvent } from "@app/signup/event/signup-user-event";
import { Injectable } from "@nestjs/common";
import { WalletsService } from "@app/wallets/service/wallets.service";

@Injectable()
export class UserWalletCreateListener {

    constructor(
        private readonly wallets: WalletsService,
    ) {}

    @OnEvent(SignupUserEvent.event)
    public async listen(event: SignupUserEvent): Promise<void> {
        const wallets = await this.wallets.getByUserId(event.user.getId());
        if(!wallets.length) {
            await this.wallets.create({
                user: event.user.getId(),
            });
        }
    }
}
