import { Injectable, NotImplementedException } from '@nestjs/common';
import { Wallet } from "@app/wallets/entity/wallet";
import { InjectWalletRepository, WalletRepositoryInterface } from "@app/wallets/repository/wallet-repository.interface";
import { UsersService } from "@app/users";
import { CreateWalletDto } from "@app/wallets/dto/create-wallet-dto";
import { EventEmitter2 } from "@nestjs/event-emitter";
import { WalletCreatedEvent } from "@app/wallets/event/wallet-created-event";
import { v4 } from "uuid";
import { DepositWalletDto } from "@app/wallets/dto/deposit-wallet-dto";
import { WalletDepositEvent } from "@app/wallets/event/wallet-deposit-event";
import { WithdrawWalletDto } from "@app/wallets/dto/withdraw-wallet-dto";
import { WalletWithdrawEvent } from "@app/wallets/event/wallet-withdraw-event";

@Injectable()
export class WalletsService {

    constructor(
        @InjectWalletRepository() private readonly wallets: WalletRepositoryInterface,
        private readonly users: UsersService,
        private readonly emitter: EventEmitter2
    ) {}

    public async create(dto: CreateWalletDto): Promise<Wallet> {
        const user = await this.users.getById(dto.user);
        const id = dto.id || v4();
        const wallet = Wallet.create(id, user, 0);
        await this.wallets.save(wallet);
        await this.emitter.emitAsync(WalletCreatedEvent.event, new WalletCreatedEvent(wallet));
        return wallet;
    }

    public async deposit(walletId: string, dto: DepositWalletDto): Promise<Wallet> {
        const wallet = await this.wallets.getById(walletId);
        wallet.deposit(dto.amount);
        await this.wallets.update(wallet);
        await this.emitter.emitAsync(WalletDepositEvent.event, new WalletDepositEvent(wallet, dto.amount));
        return wallet;
    }

    public async withdraw(walletId: string, dto: WithdrawWalletDto): Promise<Wallet> {
        const wallet = await this.wallets.getById(walletId);
        wallet.withdraw(dto.amount);
        await this.wallets.update(wallet);
        await this.emitter.emitAsync(WalletWithdrawEvent.event, new WalletWithdrawEvent(wallet, dto.amount));
        return wallet;
    }

    public async getByUserId(userId: string): Promise<Wallet[]> {
        return this.wallets.getByUserId(userId);
    }

    public async remove(walletId: string): Promise<void> {
        const wallet = await this.wallets.getById(walletId);
        await this.wallets.remove(wallet);
    }
}
