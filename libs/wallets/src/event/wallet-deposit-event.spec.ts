import { WalletDepositEvent } from './wallet-deposit-event';
import { Wallet } from "@app/wallets/entity/wallet";
import { User } from "@app/users";

describe('WalletDepositEvent', () => {
  it('should be defined', () => {
    expect(new WalletDepositEvent(Wallet.create("13", {} as User, 0), 10)).toBeDefined();
  });
});
