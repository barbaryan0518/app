import { WalletCreatedEvent } from './wallet-created-event';
import { Wallet } from "@app/wallets/entity/wallet";
import { User } from "@app/users";

describe('WalletCreatedEvent', () => {
  it('should be defined', () => {
    expect(new WalletCreatedEvent(Wallet.create("dawd", {} as User, 0))).toBeDefined();
  });
});
