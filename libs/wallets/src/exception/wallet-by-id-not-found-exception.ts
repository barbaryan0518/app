export class WalletByIdNotFoundException extends Error {
    constructor(
        public readonly id: string
    ) {
        super(`Wallet with id ${id} not found`);
    }
}
