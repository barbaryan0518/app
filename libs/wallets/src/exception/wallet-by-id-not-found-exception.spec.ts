import { WalletByIdNotFoundException } from './wallet-by-id-not-found-exception';

describe('WalletByIdNotFoundException', () => {
  it('should be defined', () => {
    expect(new WalletByIdNotFoundException("id")).toBeDefined();
  });
});
