import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsNumber } from "class-validator";

export class WithdrawWalletDto {
    @ApiProperty({
        required: true,
        type: 'number'
    })
    @Type(() => Number)
    @IsNumber()
    amount: number;
}
