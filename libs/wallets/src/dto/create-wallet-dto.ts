import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsOptional, IsString } from "class-validator";

export class CreateWalletDto {
    @ApiProperty({
        required: true,
        type: 'string',
        format: 'uuid'
    })
    @IsString()
    @Type(() => String)
    user: string;

    @ApiProperty({
        required: false,
        type: 'string',
        format: 'uuid'
    })
    @Type(() => String)
    @IsString()
    @IsOptional()
    id?: string;
}
