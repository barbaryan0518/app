import { WithdrawWalletDto } from './withdraw-wallet-dto';

describe('WithdrawWalletDto', () => {
  it('should be defined', () => {
    expect(new WithdrawWalletDto()).toBeDefined();
  });
});
