import { Controller, Get, HttpCode, Post, UseGuards } from '@nestjs/common';
import { AuthenticationService } from "@app/authentication";
import { LoginResult } from "@app/authentication/model/login-result";
import { LocalAuthGuard } from "@app/authentication/guard/local-auth-guard";
import { AuthUser } from "@app/authentication/decorator/auth-user.decorator";
import { User } from "@app/users";
import { ApiBasicAuth, ApiBearerAuth, ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { AuthGuard } from "@nestjs/passport";
import { BasicAuthGuard } from "@app/authentication/guard/basic-auth-guard";
import { JwtAuthGuard } from "@app/authentication/guard/jwt-auth-guard";

@Controller('authentication')
@ApiTags("Authentication")
export class AuthenticationController {
    constructor(
        private readonly authentication: AuthenticationService,
    ) {}

    @UseGuards(BasicAuthGuard)
    @ApiBasicAuth()
    @Post('login')
    @HttpCode(200)
    @ApiOkResponse({
        type: LoginResult,
    })
    public async login(@AuthUser() user: User): Promise<LoginResult> {
        return this.authentication.login(user);
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    @Get('info')
    @ApiOkResponse({
        type: User
    })
    public async info(@AuthUser() user: User): Promise<User> {
        return user;
    }
}
