import { INestApplication } from "@nestjs/common";
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from "@nestjs/swagger";
import { AuthenticationHttpModule } from "@app/authentication-http/authentication-http.module";

export function AuthenticationHttpSwagger(app: INestApplication): OpenAPIObject {
    const options = new DocumentBuilder()
        .setTitle("Authentication")
        .addBasicAuth()
        .addBearerAuth()
        .setVersion("1.0")
        .build();
    const document = SwaggerModule.createDocument(app, options, {
        include: [
            AuthenticationHttpModule,
        ]
    });
    SwaggerModule.setup("api/authentication/docs", app, document);
    return document;
}
