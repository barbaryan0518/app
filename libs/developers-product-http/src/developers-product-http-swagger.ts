import { INestApplication } from "@nestjs/common";
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from "@nestjs/swagger";
import { DevelopersProductHttpModule } from "@app/developers-product-http/developers-product-http.module";

export function DevelopersProductHttpSwagger(app: INestApplication): OpenAPIObject {
    const options = new DocumentBuilder()
        .setTitle("Developers Product")
        .setVersion("1.0")
        .build();
    const document = SwaggerModule.createDocument(app, options, {
        include: [
            DevelopersProductHttpModule,
        ]
    });
    SwaggerModule.setup("api/developers-product/docs", app, document);
    return document;
}
