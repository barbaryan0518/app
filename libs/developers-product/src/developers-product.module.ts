import { Module } from '@nestjs/common';
import { DevelopersProductService } from "./service/developers-product.service";
import { DEVELOPERS_PRODUCT_REPOSITORY_INTERFACE } from "@app/developers-product/repository/developers-product-repository.interface";
import { DevelopersProductRepository } from "@app/developers-product/repository/developers-product-repository";

@Module({
  providers: [
      DevelopersProductService,
      {
          provide: DEVELOPERS_PRODUCT_REPOSITORY_INTERFACE,
          useValue: DevelopersProductRepository,
      }
  ],
  exports: [DevelopersProductService],
})
export class DevelopersProductModule {}
