import { Connection, Repository } from "typeorm";
import { Injectable } from "@nestjs/common";
import { DeveloperProduct, DevelopersProductRepositoryInterface } from "@app/developers-product";
import { DeveloperProductByIdNotFoundException } from "@app/developers-product/exception/developer-product-by-id-not-found-exception";

@Injectable()
export class DevelopersProductRepository implements DevelopersProductRepositoryInterface{
    constructor(
        private readonly connection: Connection
    ) {}

    private get repository(): Repository<DeveloperProduct> {
        return this.connection.getRepository(DeveloperProduct);
    }

    public async find(): Promise<DeveloperProduct[]> {
        return await this.repository.find();
    }

    public async getById(id: string): Promise<DeveloperProduct> {
        const found = await this.repository.findOne(id);
        if(!found) {
            throw new DeveloperProductByIdNotFoundException(id);
        }
        return found;
    }

    public async findByIds(ids: string[]): Promise<DeveloperProduct[]> {
        return await this.repository.findByIds(ids);
    }

    public async remove(developerProduct: DeveloperProduct): Promise<void> {
        await this.repository.remove(developerProduct);
    }

    public async save(developerProduct: DeveloperProduct): Promise<void> {
        await this.repository.save(developerProduct);
    }

    public async update(developerProduct: DeveloperProduct): Promise<void> {
        await this.repository.save(developerProduct);
    }
}
