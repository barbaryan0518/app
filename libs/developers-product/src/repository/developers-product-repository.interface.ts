import { Inject } from "@nestjs/common";
import { DeveloperProduct } from "../entity/developer-product";

export interface DevelopersProductRepositoryInterface {
    save(developerProduct: DeveloperProduct): Promise<void>;
    update(developerProduct: DeveloperProduct): Promise<void>;
    remove(developerProduct: DeveloperProduct): Promise<void>;
    find(): Promise<DeveloperProduct[]>;

    /**
     * @throws DeveloperProductByIdNotFoundException
     */
    getById(id: string): Promise<DeveloperProduct>;

    findByIds(ids: string[]): Promise<DeveloperProduct[]>;
}

export const DEVELOPERS_PRODUCT_REPOSITORY_INTERFACE = "DevelopersProductRepositoryInterface";

export const InjectDevelopersProductRepository = Inject.bind(null, DEVELOPERS_PRODUCT_REPOSITORY_INTERFACE);
