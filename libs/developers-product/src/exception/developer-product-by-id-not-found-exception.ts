export class DeveloperProductByIdNotFoundException extends Error {
    constructor(public readonly id: string) {
        super(`Developer product by id ${id} not found`);
    }
}
