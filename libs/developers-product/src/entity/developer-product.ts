import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { Exclude, Expose } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

@Entity("developers_product")
export class DeveloperProduct {

    @PrimaryGeneratedColumn('uuid')
    @Exclude()
    private readonly id: string;

    @Column()
    @Exclude()
    private name: string;

    @Column()
    @Exclude()
    private avatar: string;

    @Column({
        type: 'int'
    })
    @Exclude()
    private performance: number;

    @Column({
        type: 'numeric',
        precision: 10,
        scale: 2
    })
    @Exclude()
    private price: number;

    constructor(id: string, name: string, avatar: string, performance: number, price: number) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.performance = performance;
        this.price = price;
    }

    @ApiProperty({
        type: 'string',
        name: 'id',
        format: 'uuid'
    })
    @Expose({
        name: 'id'
    })
    public getId(): string {
        return this.id;
    }

    @ApiProperty({
        type: 'string',
        name: 'name'
    })
    @Expose({
        name: 'name'
    })
    public getName(): string {
        return this.name;
    }

    @ApiProperty({
        type: 'string',
        name: 'avatar'
    })
    @Expose({
        name: 'avatar'
    })
    public getAvatar(): string {
        return this.avatar;
    }

    @ApiProperty({
        type: 'number',
        name: 'performance'
    })
    @Expose({
        name: 'performance'
    })
    public getPerformance(): number {
        return this.performance;
    }

    @ApiProperty({
        type: 'number',
        name: 'price'
    })
    @Expose({
        name: 'price'
    })
    public getPrice(): number {
        return this.price;
    }

    public changeName(name: string): void {
        this.name = name;
    }

    public changeAvatar(avatar: string): void {
        this.avatar = avatar;
    }

    public changePerformance(performance: number): void {
        this.performance = performance;
    }

    public changePrice(price: number): void {
        this.price = price;
    }
}
