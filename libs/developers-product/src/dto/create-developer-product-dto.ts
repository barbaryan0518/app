import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsNumber, IsString } from "class-validator";

export class CreateDeveloperProductDto {
    @ApiProperty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsString()
    avatar: string;

    @ApiProperty({
        type: 'number'
    })
    @IsInt()
    performance: number;

    @ApiProperty()
    @IsNumber()
    price: number;
}
