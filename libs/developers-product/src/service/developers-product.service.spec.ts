import { Test, TestingModule } from '@nestjs/testing';
import { DevelopersProductService } from "./developers-product.service";
import {
    DEVELOPERS_PRODUCT_REPOSITORY_INTERFACE,
    DevelopersProductRepositoryInterface
} from "../repository/developers-product-repository.interface";
import { CreateDeveloperProductDto } from "../dto/create-developer-product-dto";
import { DeveloperProduct } from "./../entity/developer-product";


describe('DevelopersProductService', () => {
    let service: DevelopersProductService;
    let developersProductRepository: DevelopersProductRepositoryInterface;

    beforeEach(async () => {
        developersProductRepository = {} as DevelopersProductRepositoryInterface;
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                DevelopersProductService,
                {
                    provide: DEVELOPERS_PRODUCT_REPOSITORY_INTERFACE,
                    useFactory: () => developersProductRepository
                }
            ],
        }).compile();

        service = module.get<DevelopersProductService>(DevelopersProductService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    describe("create", () => {
        it("should be create developer product", async () => {
            const dto = {
                price: 222,
                performance: 100,
                avatar: 'https://same.com',
                name: 'app.name'
            } as CreateDeveloperProductDto;
            developersProductRepository.save = jest.fn();
            const developerProduct = await service.create(dto);
            expect(developerProduct.getId()).toBeDefined();
            expect(developersProductRepository.save).toHaveBeenCalled();
            expect(developerProduct.getAvatar()).toBe(dto.avatar);
            expect(developerProduct.getName()).toBe(dto.name);
            expect(developerProduct.getPerformance()).toBe(dto.performance);
            expect(developerProduct.getPrice()).toBe(dto.price);
        });
    });

    describe("find", () => {
        it("should be return founded products", async () => {
            const products = [
                new DeveloperProduct("123", "222", "333", 22, 11),
                new DeveloperProduct("1223", "2221", "3343", 222, 111),
            ];
            developersProductRepository.find = jest.fn().mockImplementation(async () => products);
            const developerProducts = await service.find();
            expect(developerProducts).toEqual(products);
        });
    });

    describe("getById", () => {
        it("should be return product", async () => {
            const product = new DeveloperProduct("123", "222", "333", 22, 11);
            developersProductRepository.getById = jest.fn().mockImplementation(async (id: string) => {
                expect(id).toBe("123");
                return product;
            });
            const developerProducts = await service.getById('123');
            expect(developerProducts).toEqual(product);
            expect(developersProductRepository.getById).toHaveBeenCalled();
        });
    });

    describe("remove", () => {
        it("should be remove return developer", async () => {
            const productId = '123444';
            const expectedDeveloperProduct = new DeveloperProduct(
                "113",
                "123",
                "123",
                100,
                304
            );
            developersProductRepository.getById = jest.fn().mockImplementation(async id => {
                expect(id).toBe(productId);
                return expectedDeveloperProduct;
            });
            developersProductRepository.remove = jest.fn().mockImplementation(async entity => {
                expect(entity).toEqual(expectedDeveloperProduct);
            });
            await service.remove(productId);
            expect(developersProductRepository.getById).toHaveBeenCalled();
            expect(developersProductRepository.remove).toHaveBeenCalled();
        });
    })
});
